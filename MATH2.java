import java.util.Scanner;

public class MATH2 {
	public static void main(String [] args){
		Scanner keyboard = new Scanner(System.in);
		double H, F, G, x, y, z;
		double powA, powB;
		int i;
		
		//Prompt the user to type the x, y, z values
		System.out.print("x: ");
		x = keyboard.nextDouble();
		System.out.print("y: ");
		y = keyboard.nextDouble();
		System.out.print("z: ");
		z = keyboard.nextDouble();
		
		//Formula H
		powA = x;
	    powB = 10;
	    H = 1;
	    for(i=1; i<=powB; i++)	H *= powA;
	    
	    //Formula F
		F = x+y;
		
		//Formula G
		//G_sqrt
		double squareRoot = x/2;
		double G_sqrt;
		do {
			G_sqrt = squareRoot;
			squareRoot = (G_sqrt + (x / G_sqrt)) / 2;
		} while ((G_sqrt - squareRoot) != 0);
		
		//G_abs
		if(y<0) y = -y;
		double G_abs = y;
		
		//G_pow
		powA = z;
		powB = y;
		double G_pow = 1;
		for (i=1; i<=powB; i++) G_pow *= powA;
		
		G = G_sqrt + G_abs + G_pow;
		
		
		//Result
		System.out.println("\n<RESULT>\n" + "h(x)= " + H + "\n" + "f(x,y)= " + F + "\n" + "g(x,y,z)= " + G);
	}
}