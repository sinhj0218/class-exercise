/**
 * Room class is to check each information of the room
 * 
 * @author Hyejung Shin
 *
 */
public class Room {
	//instance variables
	private String paint;
	private String floor;
	private int window;
	
	/**
	 * Initialized room values to default.
	 */
	public Room(){
		this.paint = "";
		this.floor = "";
		this.window = 0;
	}
	
	/**
	 * Initialized room values
	 * @param paint
	 * @param floor
	 * @param window
	 */
	public Room(String paint, String floor, int window){
		this.paint = paint;
		this.floor = floor;
		this.window = window;
	}
	
	/**
	 * wall color
	 * @param paint
	 */
	public void setPaint(String paint){
		this.paint = paint;
	}
	
	/**
	 * @return
	 */
	public String getPaint(){
		return this.paint;
	}
	
	/**
	 * floor type
	 * @param floor
	 */
	public void setFloor(String floor){
		this.floor = floor;
	}
	
	/**
	 * @return
	 */
	public String getFloor(){
		return this.floor;
	}
	
	/**
	 * window number
	 * @param window
	 */
	public void setWindow(int window){
		this.window = window;
	}
	
	/**
	 * @return
	 */
	public int getWindow(){
		return this.window;
	}

	/**
	 * Return the room information
	 */
	public String toString(){
		return "The room is painted " + this.paint + " with " + this.floor + " flooring and " + this.window + " window(s)."; 
	}
}