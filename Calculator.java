
public class Calculator {
	static double sum = 0.0;
	static int i;
	
	public static double getSum(double [] num){
		for(i=0; i<num.length; i++){
			sum += num[i];
		}
		return sum;
	}
	
	public static double getAverage(double [] num){
		for(i=0; i<num.length; i++){
			sum += num[i];
		}
		return (sum/num.length);
	}
	
	public static double getProduct(double [] num){
		double pro = 1.0;
		for(i=0; i<num.length; i++){
			pro *= num[i];
		}
		return pro;
	}
}
