import java.util.Scanner;

public class WHILE {
	public static void main(String [] args){
		Scanner keyboard = new Scanner(System.in);
		String ID, PW;
		int i=1;
		
		System.out.print("Enter your username: " );
		ID = keyboard.next();
		System.out.print("Enter your password: " );
		PW = keyboard.next();
		
		if((ID.equals("hs2256")) & (PW.equals("hi123")))
			System.out.println("Welcome "+ID+"!");
		else {
			while (i<3){
				System.out.println("Wrong username or password. Please enter again!");
				System.out.print("Enter your username: " );
				ID = keyboard.next();
				System.out.print("Enter your password: " );
				PW = keyboard.next();
				i++;
				
				if((ID.equals("hs2256")) & (PW.equals("hi123"))){
					i=4;
					System.out.println("Welcome "+ID+"!");
				}
			}
			if(i==3) System.out.println("Please contact your adminstrator to unlock your account!");
		}
	}
}
