import java.util.Scanner;

public class Rectangle {
	public static void main(String [] args){
		Scanner keyboard = new Scanner(System.in);
		double height, width, perimeter, area;
		
		System.out.print("Enter height of the rectangle: ");
		height = keyboard.nextDouble();
		System.out.print("Enter width of the rectangle: ");
		width = keyboard.nextDouble();
		
		perimeter = 2*(height+width);
		area = height*width;
		
		System.out.println("The perimeter of the rectangle is " + perimeter);
		System.out.println("The area of the rectangle is " + area);		
	}
}