import javax.swing.JOptionPane;
import java.util.Scanner;

public class Assignment3{
	public static void main(String [] args){
		Scanner keyboard = new Scanner(System.in);
		
		String AdminID, AdminPW, StudentID, StudentPW, StaffID, StaffPW;
		String input;
		int i, j;
		
		//Choice box of account type 
		String[] choices = {"Admin", "Student", "Staff"};
		input = (String) JOptionPane.showInputDialog(null, "Choose account type:",
				"Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]);
		
		switch(input){
		case "Admin":				//if the user chooses Admin as an account type
			for (i=1; i<4; i++){	//3 username trials of limitation
				AdminID = JOptionPane.showInputDialog(null, "Enter your username:");
				if(AdminID.equals("admin")){
					for (j=1; j<4 ; j++){	//3 password trials of limitation 
						AdminPW = JOptionPane.showInputDialog(null, "Enter your password:");
						if (AdminPW.equals("admin123")){
							JOptionPane.showMessageDialog(null, "Welcome " + input + "!\nYou can update and read file.");
							return;
						}
						else{		//if password is wrong, error message box will pop up
							JOptionPane.showMessageDialog(null, "The password is wrong!");
							i=4;	//when the password is wrong for 3 times, it can exit the first FOR loop and display error message
						}
					}
				}
				else if(AdminID.equals("student")|AdminID.equals("staff")){	//if the user types student or staff as username, go back to choice box
					input = (String) JOptionPane.showInputDialog(null, "Choose account type:",
							"Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]);
					i=1; 			//not to do count trial
				}
				else{				//error message
					JOptionPane.showMessageDialog(null, "Invalid Username!");
					
				}
			}
			//expiration of 3 trials
			JOptionPane.showMessageDialog(null, "Your account is locked! \nContact to administrator.");
			break;
		case "Student":				//if the user chooses Student as an account type
			for (i=1; i<4; i++){
				StudentID = JOptionPane.showInputDialog(null, "Enter your username:");
				if(StudentID.equals("student")){
					for (j=1; j<4 ; j++){
						StudentPW = JOptionPane.showInputDialog(null, "Enter your password:");
						if (StudentPW.equals("student123")){
							JOptionPane.showMessageDialog(null, "Welcome " + input + "!\nYou can only read file.");
							return;
						}
						else{
							JOptionPane.showMessageDialog(null, "The password is wrong!");
							i=4;
						}
					}
				}
				else if(StudentID.equals("admin")|StudentID.equals("staff")){
					input = (String) JOptionPane.showInputDialog(null, "Choose account type:",
							"Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]);
					i=1;
				}
				else{
					JOptionPane.showMessageDialog(null, "Invalid Username!");
				}
			}
			JOptionPane.showMessageDialog(null, "Your account is locked! \nContact to administrator.");
			break;
		case "Staff":				//if the user chooses Staff as an account type
			for (i=1; i<4; i++){
				StaffID = JOptionPane.showInputDialog(null, "Enter your username:");
				if(StaffID.equals("staff")){
					for (j=1; j<4 ; j++){
						StaffPW = JOptionPane.showInputDialog(null, "Enter your password:");
						if (StaffPW.equals("staff123")){
							JOptionPane.showMessageDialog(null, "Welcome " + input + "!\nYou can update, read, add, delete file.");
							return;
						}
						else{
							JOptionPane.showMessageDialog(null, "The password is wrong!");
							i=4;
						}
					}
				}
				else if(StaffID.equals("admin")|StaffID.equals("student")){
					input = (String) JOptionPane.showInputDialog(null, "Choose account type:",
							"Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]);
					i=1;
				}
				else{
					JOptionPane.showMessageDialog(null, "Invalid Username!");
				}
			}
			JOptionPane.showMessageDialog(null, "Your account is locked! \nContact to administrator.");
			break;
		}
	}
}