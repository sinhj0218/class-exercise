import java.util.Scanner;

public class Assignment1 {
	public static void main(String[] args){
		double radius, depth, water;
		double PI = 3.14159;
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("1. Enter the radius of the well in inches:");
		radius = keyboard.nextDouble();
		System.out.println("2. Enter the depth of the well in feet:");
		depth = keyboard.nextDouble();
		
		water = (radius/12) * (radius/12) * depth * PI * 7.48;
		
		System.out.println("A " + depth + " foot well full of water with a radius of " + radius + " inches for the casing "
				+ "holds about " + Math.round(water) + " gallons of water.");
	}
}
