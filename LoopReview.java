import java.util.Scanner;

public class LoopReview {
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);
		int Odd=1;
		int Even=2;
		int j=0;

		//Odd
		System.out.print("Odd output: ");
		for (int i=0; i<100; i++){
			System.out.print(Odd+" ");
			Odd = Odd+2;		
		}
		
		//Even
		System.out.print("\nEven output: ");
		do{
			System.out.print(Even+" ");
			Even = Even+2;
			j++;
		} while (j<100);
		System.out.print("\n");

		//Diamond
		int m=1, n=0;
		while(m<6){
			while(n<9){
				System.out.print(" ");
				n+=2;
			}
			
			n=0;
			while(n<m){
				System.out.print("*");
				n++;
			}
			
			System.out.print("\n");
			m+=2;
		}

		m=3; n=0;
		while(m>0){
			while(n<7){
				System.out.print(" ");
				n+=2;
			}
			
			n=0;
			while(n<m){
				System.out.print("*");
				n++;
			}
			
			System.out.print("\n");
			m-=2; n=-2;
		}
	}
}