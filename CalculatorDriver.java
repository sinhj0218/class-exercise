
public class CalculatorDriver {
	public static void main(String [] args){
		double [] num = new double [3];
		num[0] = 3.4;
		num[1] = 15.0;
		num[2] = 0.5;
		
		System.out.print("total of numbers is " + Calculator.getSum(num));
		System.out.print("\naverage of numbers is " + Calculator.getAverage(num));
		System.out.print("\nproduct of numbers is " + Calculator.getProduct(num));
	}
}
